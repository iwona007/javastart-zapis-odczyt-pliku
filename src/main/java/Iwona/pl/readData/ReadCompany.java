package Iwona.pl.readData;

import Iwona.pl.model.Company;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.ObjectInputStream;

public class ReadCompany {

    private final static String FILE_NAME = "employee.obj";

    public void readData() {
        try (
                FileInputStream fis = new FileInputStream(FILE_NAME);
                ObjectInputStream ois = new ObjectInputStream(fis);
        ) {
            Company company1 = (Company) ois.readObject();
            System.out.println("Odczytanie obiektów z pliku");
            System.out.println(company1);
        } catch (IOException | ClassNotFoundException e) {
            e.printStackTrace();
        }
    }
}



