package Iwona.pl.controller;

import Iwona.pl.readData.ReadCompany;
import Iwona.pl.writeData.WriteCompany;
import java.util.Scanner;

public class DataController {

    private Scanner input = new Scanner(System.in);
    private static final int WRITE_DATA = 1;
    private static final int READ_DATA = 2;

    private ReadCompany readCompany;
    private WriteCompany writeCompany;

    public DataController() {
        this.readCompany = new ReadCompany();
        this.writeCompany = new WriteCompany();
    }

    public void choseOption() {
        System.out.println("Wprowadzenie danych: " + WRITE_DATA);
        System.out.println("Ddczytanie danych: " + READ_DATA);

        int choose = input.nextInt();
        input.nextLine();

        if (choose == WRITE_DATA) {
            writeCompany.writeData();
        } else if (choose == READ_DATA) {
            readCompany.readData();
        }
        input.close();
    }
}
