package Iwona.pl.model;

import java.math.BigDecimal;

public class Employee extends Person {

    private BigDecimal salary;

    public Employee (String firstNAme, String lastName, BigDecimal salary){
        super(firstNAme, lastName);
        this.salary = salary;
    }

    public BigDecimal getSalary() {
        return salary;
    }

    public void setSalary(BigDecimal salary) {
        this.salary = salary;
    }

    @Override
    public String toString() {
        StringBuilder builder = new StringBuilder();
        builder.append("Imię pracownika: ").append(getFirstName());
        builder.append(" Nazwisko praconika: ").append(getLastName());
        builder.append(" wyplata: ").append(getSalary());
        return builder.toString();
    }
}
