package Iwona.pl.writeData;

import Iwona.pl.model.Company;
import Iwona.pl.model.Employee;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.ObjectOutputStream;
import java.math.BigDecimal;
import java.util.InputMismatchException;
import java.util.NoSuchElementException;
import java.util.Scanner;

public class WriteCompany {
    Scanner input = new Scanner(System.in);
    private static final String FILE_NAME = "employee.obj";

    public void writeData() {
        try (
                FileOutputStream fos = new FileOutputStream(FILE_NAME);
                ObjectOutputStream oos = new ObjectOutputStream(fos);
        ) {
            Company company = createEmployee();
            oos.writeObject(company);
            System.out.println("Zapisano obiekt do pliku");
        } catch (IOException e) {
            System.err.println("Bład zapisu pliku " + FILE_NAME);
            e.printStackTrace();
        } catch (InputMismatchException e){
            System.err.println("Podałeś blędny format danych");
            e.printStackTrace();
        }
    }

    private Company createEmployee() {
        Company company = new Company();
        for (int i = 0; i < Company.MAX_EMPLOYEES; i++) {
            System.out.println("Podaj imie pracownika: ");
            String firstName = input.nextLine();

            System.out.println("Podaj nazwisko pracownika: ");
            String lastName = input.nextLine();

            System.out.println("Podaj wynagrodzenie pracownika: ");
            BigDecimal salary = input.nextBigDecimal();
            input.nextLine();

            company.add(new Employee(firstName, lastName, salary));
        }
        return company;
    }
}
